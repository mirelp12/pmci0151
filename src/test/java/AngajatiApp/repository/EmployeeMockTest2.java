package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static AngajatiApp.controller.DidacticFunction.ASISTENT;
import static AngajatiApp.controller.DidacticFunction.LECTURER;
import static org.junit.Assert.*;

public class EmployeeMockTest2 {

    private EmployeeMock eM;

    @Before
    public void setUp() throws Exception {
        eM = new EmployeeMock();
    }

    @After
    public void tearDown() throws Exception {
        eM = null;
    }


    @Test
    public void TC1() {
        Employee e1 = null;
        List<Employee> employeeMockList = eM.getEmployeeList();
        eM.modifyEmployeeFunction(e1, DidacticFunction.LECTURER);
        assertTrue(eM.getEmployeeList().equals(employeeMockList));
    }


    @Test
    public void TC2() {
        Employee e1 = new Employee("Ion", "Vasile", "1234567123456", DidacticFunction.ASISTENT, 2500d);
        e1.setId(99);

        String employeeMockList = eM.getEmployeeList().toString();
        for (Employee e : eM.getEmployeeList()) {
            if (e.equals(e1)) {
                e1.setId(e.getId());
            }
        }
        ;
        eM.modifyEmployeeFunction(e1, DidacticFunction.LECTURER);
        assertTrue(eM.getEmployeeList().toString().equals(employeeMockList));
    }


    @Test
    public void TC3() {
        Employee e1 = new Employee("Marius", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2500d);
        e1.setId(0);

        String employeeMockList = eM.getEmployeeList().toString();
        for (Employee e : eM.getEmployeeList()) {
            if (e.equals(e1)) {
                e1.setId(e.getId());
            }
        }
        ;
        eM.modifyEmployeeFunction(e1, DidacticFunction.LECTURER);
        assertNotEquals(eM.getEmployeeList().toString(), employeeMockList);
    }

    @Test
    public void TC4() {
        Employee e1 = new Employee("Marius", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2500d);
        e1.setId(99);

        eM.getEmployeeList().clear();
        String employeeMockList = eM.getEmployeeList().toString();

        for (Employee e : eM.getEmployeeList()) {
            if (e.equals(e1)) {
                e1.setId(e.getId());
            }
        }
        ;

        eM.modifyEmployeeFunction(e1, DidacticFunction.LECTURER);
        assertTrue(eM.getEmployeeList().toString().equals(employeeMockList));
    }
}