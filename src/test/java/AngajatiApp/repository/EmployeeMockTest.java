package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import org.junit.After;
import org.junit.Before;
import AngajatiApp.model.Employee;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static AngajatiApp.controller.DidacticFunction.ASISTENT;
import static org.junit.Assert.*;
import static org.junit.Assert.*;
public class EmployeeMockTest {
    private EmployeeMock employeeMock = new EmployeeMock();
    DidacticFunction df = ASISTENT;
    @Before
    public void setUp(){
        EmployeeMock employeeMock = new EmployeeMock();
    }

    @After
    public void tearDown(){
        employeeMock = null;
    }


    @Test
    public void TC1() {
        Employee testEmployee = new Employee ("Ion", "Popescu", "1111111111111",df, 0.01);
        boolean result1 = employeeMock.addEmployee(testEmployee);
        assertTrue(result1);
    }

    @Test
    public void TC2() {
        Employee testEmployee = new Employee ("Amalia", "Enescu", "abc",df, 0.00);
        boolean result2 = employeeMock.addEmployee(testEmployee);
        assertFalse(result2);
    }

    @Test
    public void TC4() {
        Employee testEmployee = new Employee ("Andrei", "Antonescu", "1111111111111",df, 0.00);
        boolean result4 = employeeMock.addEmployee(testEmployee);
        assertFalse(result4);
    }

    @Test
    public void TC5() {
        Employee testEmployee = new Employee ("Mihaela", "Enescu", "222222222222",df, 0.01);
        boolean result5 = employeeMock.addEmployee(testEmployee);
        assertFalse(result5);
    }

    @Test
    public void TC6() {
        Employee testEmployee = new Employee ("Ionel", "Georgescu", "111111111111",df, 0.00);
        boolean result6 = employeeMock.addEmployee(testEmployee);
        assertFalse(result6);
    }

    @Test
    public void TC7() {
        Employee testEmployee = new Employee ("Ionel", "Popescu", "1111111111111",df, 10000.00);
        boolean result7 = employeeMock.addEmployee(testEmployee);
        assertTrue(result7);
    }


}