package AngajatiApp.model;

import org.junit.*;

import static org.junit.Assert.*;

public class EmployeeTest {
    private Employee c1;
    private Employee c2;
    private Employee c3;
    private Employee c4;

    //se executa automat inaintea fiecarei metode de test
    @Before
    public void setUp() {
        c1 = new Employee();//Ion);
        c1.setFirstName("Ion");
        c2 = new Employee();//Vasile);
        c2.setFirstName("Vasile");
        c3 = new Employee();
        c4 = null;

        System.out.println("Before test");
    }

    //se executa automat dupa fiecarei metoda de test
    @After
    public void tearDown() {
        c1 = null;
        c2 = null;
        c3 = null;

        System.out.println("After test");
    }

    @Test
    public void testGetTitlu() {
        assertEquals("Ion", c1.getFirstName());
    }

    @Test (expected=NullPointerException.class)
    public void testGetTitlu2() {
        assertEquals("Vasile", c4.getFirstName());
    }

//    @Test (timeout=100) //asteapta 10 milisecunde
//    public void testFictiv(){
//        try {
//            Thread.sleep (100);
//        } catch (InterruptedException e){
//            e.printStackTrace();
//        }
//    }

    @Test
    public void testConstructor(){
        assertNotEquals("verificam daca s-a creat cartea 1",c1,null);
    }

    @BeforeClass
    public static void setUpAll(){
        System.out.println("Before All tests - at the beginning of the Test Class");
    }

    @AfterClass
    public static void tearDownAll(){
        System.out.println("After All tests - at the end of the Test Class");
    }
}